import socket
import numpy
import time
import cv2

UDP_IP = "192.168.1.90"
UDP_PORT = 51137

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind ((UDP_IP, UDP_PORT))

s=""
last=""

while True:

    data, addr = sock.recvfrom(10000)

    if not last == data:
        s += data
        print "times: ", len(s) / 46080, "length: ", len(s)

    last = data

    if len(s) >= (30000):

        frame = numpy.fromstring (s,dtype=numpy.uint8)
        frame = frame.reshape (20,20,3)

        cv2.imshow('frame',frame)

        s=""
        cv2.waitKey()
        cv2.destroyWindow('frame')
        #break
